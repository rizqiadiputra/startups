from django.contrib import admin
from .models import *
# Register your models here.

class ProgramAdmin(admin.ModelAdmin):
    list_display = ('name','description')
admin.site.register(Program, ProgramAdmin)

class SkillDataAdmin(admin.ModelAdmin):
    list_display = ('name',)
admin.site.register(SkillData, SkillDataAdmin)

class ExperienceDataAdmin(admin.ModelAdmin):
    list_display = ('title','company','is_current', 'id')
admin.site.register(ExperienceData, ExperienceDataAdmin)

class EducationDataAdmin(admin.ModelAdmin):
    list_display = ('name','department','year')
admin.site.register(EducationData, EducationDataAdmin)