import json
import datetime
import uuid
from django.utils import timezone
from django.db import models
from django.utils.text import slugify
from ckeditor.fields import RichTextField
from django.urls import reverse

class Program(models.Model):
  name = models.CharField(max_length=255)
  description = models.TextField()
  slug = models.SlugField(max_length=128, blank=True)

  def __str__(self):
    return self.slug

  def save(self, *args, **kwargs):
    if self.slug is '' or self.slug is None:
      self.slug = slugify(self.name)
    super().save(*args, **kwargs)

"""
COMMON DATA
"""

class SkillData(models.Model):
  id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
  profile = models.ForeignKey('ecosystem.Profile', blank=True, null=True, on_delete=models.CASCADE)
  name = models.CharField(max_length=255,)

  def __str__(self):
      return self.name

  def get_absolute_url(self):
    return reverse('myprofile', args=[str(self.profile.slug)])
  
class ExperienceData(models.Model):
  id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
  profile = models.ForeignKey('ecosystem.Profile', blank=True, null=True, on_delete=models.CASCADE)
  title = models.CharField(max_length=255,)
  company = models.CharField(max_length=255, )
  location = models.URLField(max_length=255, blank=True)
  enroll_date = models.DateField(blank=True, null=True, default=timezone.now,)
  graduate_date = models.DateField(blank=True, null=True, default=timezone.now,)
  description = RichTextField(blank=True)
  is_current = models.BooleanField(default=False)

  def __str__(self):
      return self.title + ' -- ' + self.company

  def get_absolute_url(self):
    return reverse('myprofile', args=[str(self.profile.slug)])

class EducationData(models.Model):
  id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
  profile = models.ForeignKey('ecosystem.Profile', blank=True, null=True, on_delete=models.CASCADE)
  name = models.CharField(max_length=255,)
  department = models.CharField(max_length=255,)
  year = models.PositiveIntegerField()

  def __str__(self):
      return self.name + ' -- ' + self.department + ' -- ' + self.year.__str__()

  def get_absolute_url(self):
    return reverse('myprofile', args=[str(self.profile.slug)])