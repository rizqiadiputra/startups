from django.contrib.auth.models import User
from django.views.generic import TemplateView, DetailView, RedirectView
from ecosystem.models import *
from journal.models import *
import datetime as dt
from datetime import timedelta
from django.db.models import Sum, Max, Q
from django.shortcuts import redirect, get_object_or_404
from django.contrib.auth import authenticate
from django.contrib.sessions.models import Session

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from django.utils import timezone
from django.http import HttpResponseRedirect
from django.contrib.messages import warning

@method_decorator(login_required, name='dispatch')
class HomeView(TemplateView):
  template_name = 'index.html' 

  """
  redirect view: homeview/role-startup
  """
  def dispatch(self, *args, **kwargs):

    user = self.request.user
    staff = user.is_staff
    cek_startup = user.startupmember_set.all()
    startupmember = len(cek_startup) < 1
    bukanstaff = staff == False 
    cekmentor = hasattr(user,'mentor')
    mentor = cekmentor == False

    cek = startupmember and bukanstaff and mentor
    if cek:
      # return redirect('role-startup')
      return redirect('profile_create', slug=user.profile.slug)
      # return redirect('profile_create', args=[str(self.request.user.id)])


    return super(HomeView, self).dispatch(*args, **kwargs)

  def get_context_data(self, *args, **kwargs):
    context = super(HomeView, self).get_context_data(**kwargs)
    
    """
    progress startupsuccess
    """
    context['startupData'] = Startup.objects.all().count()
    context['mentorData'] = Mentor.objects.all().count()
    context['userData'] = Profile.objects.all().count()
    context['maleData'] = Profile.objects.filter(gender='MALE').count()
    context['femaleData'] = Profile.objects.filter(gender='FEMALE').count()
  

    """
    startup update and non update per week
    """
    date = timezone.now()
    week = date - timedelta(days=7)

    temp = Startup.objects.all().annotate(total_revenue=Sum('startupprogress__revenue')).annotate(total_acquisition=Sum('startupprogress__acquisition')).annotate(total_activation=Sum('startupprogress__activation')).annotate(last_week=Max('startupprogress__week'))
    updateData = temp.filter(startupprogress__datetime__range = [week, date])
    context['updateData'] = updateData
    nonupdateData = temp
    for update in updateData:
      nonupdateData = nonupdateData.values().filter(~Q(name = update.name))
    context['nonupdateData'] = nonupdateData
    
    """
    best startup and mentor
    """
    best_startup =  StartupProgress.objects.select_related('startup').order_by('-revenue')
    if len(best_startup) < 1:
      context['bestStartup'] = None
    else:
      context['bestStartup'] = best_startup[0]

    best_mentor = FeedbackForMentor.objects.select_related('mentoring').order_by('-helpfullness_grade')
    if len(best_mentor) < 1:
      context['bestMentor'] = None
    else:
      context['bestMentor'] = best_mentor[0]

    return context      

  def absolute(request):
    urls = {
        'ABSOLUTE_ROOT': request.build_absolute_uri('/')[:-1].strip("/"),
        'ABSOLUTE_ROOT_URL': request.build_absolute_uri('/').strip("/"),
    }

    return urls
