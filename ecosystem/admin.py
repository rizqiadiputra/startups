from django.contrib import admin
from ecosystem.models import *
from django.contrib.auth.models import User

class ProfileAdmin(admin.ModelAdmin):
  list_display = ('__str__','user',)
admin.site.register(Profile, ProfileAdmin)

class MentorAdmin(admin.ModelAdmin):
  list_display = ('mentor',)
admin.site.register(Mentor, MentorAdmin)

class StartupAdmin(admin.ModelAdmin):
  list_display = ('name',)
admin.site.register(Startup, StartupAdmin)

class StartupMemberAdmin(admin.ModelAdmin):
  list_display = ('member',)
admin.site.register(StartupMember, StartupMemberAdmin)

class RequestStartupMemberAdmin(admin.ModelAdmin):
  list_display = ('user_rq','startup_rq','approval','created')
admin.site.register(RequestStartupMember, RequestStartupMemberAdmin)

class QualificationAskAdmin(admin.ModelAdmin):
  list_display = ('pertanyaan',)
admin.site.register(QualificationAsk, QualificationAskAdmin)

class QualificationOptionAdmin(admin.ModelAdmin):
  list_display = ('mentor_pertanyaan','opsi')
admin.site.register(QualificationOption, QualificationOptionAdmin)

class QualificationAnswerAdmin(admin.ModelAdmin):
  list_display = ('user_mentor','jawaban',)
admin.site.register(QualificationAnswer, QualificationAnswerAdmin)
