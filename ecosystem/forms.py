from django import forms
from .models import *
from django.forms import ModelForm

# class ProfileForm(ModelForm):
#     class Meta:
#         model = Profile
#         fields = '__all__'

class ExperienceForm(forms.ModelForm):
    class Meta:
        model = ExperienceData
        fields = ['title','company','location','enroll_date','graduate_date','description','is_current',]

class EducationForm(forms.ModelForm):
    class Meta:
        model = EducationData
        fields = ['name','department','year',]

class SkillForm(forms.ModelForm):
    class Meta:
        model = SkillData
        fields = ['name',]

class ProfileUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields =  ['first_name','last_name','email',]

class ProfileForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
    
        self.user = kwargs['instance'].user
        user_kwargs = kwargs.copy()
        user_kwargs['instance'] = self.user
        self.puf = ProfileUserForm(*args, **user_kwargs)

        super(ProfileForm, self).__init__(*args, **kwargs)

        self.fields.update(self.puf.fields)
        self.initial.update(self.puf.initial)


        self.fields.keyOrder = (
            'first_name',
            'last_name',
            'email',

            'slug',
            'birthdate',
            'gender',
            'bio',
            'phone_number',
            'facebook_profile',
            'linkedin_profile',
            'picture',
            'header_image',

        )

    def save(self, *args, **kwargs):
        self.puf.save(*args, **kwargs)
        return super(ProfileForm, self).save(*args, **kwargs)

    class Meta:
        model = Profile
        exclude = ()

# class AnswerForm(forms.ModelForm):
#     class Meta:
#         model = QualificationAnswer
#         fields = 'jawaban,'
#         exlude = ()