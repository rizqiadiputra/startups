import json
import datetime
import uuid
from django.utils import timezone
from django.db import models
from django.db.models.signals import m2m_changed
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.auth.models import User
from django.dispatch import receiver, Signal
from django.utils.text import slugify
from allauth.account.signals import user_signed_up
from ckeditor.fields import RichTextField
from django.db.models import Avg
from django.urls import reverse
from util.randomizer import unique_slug_generator, unique_slug_generator_byfield
from config.models import *
from journal.models import FeedbackForMentor
from django.db.models.signals import post_save, pre_save
from invitations.signals import invite_accepted
from invitations.models import Invitation
from django.contrib import auth
from allauth.account.signals import user_signed_up
from allauth.account.models import EmailAddress

"""
PROFILE
"""

MALE = 'MALE'
FEMALE = 'FEMALE'
OTHER = 'OTHER'
GENDER_CHOICES = (
  (MALE, 'Male'),
  (FEMALE, 'Female'),
)

def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user/<username>/<filename>
    return 'user/{0}/{1}'.format(instance.user.username, filename)

class Profile(models.Model):
  user = models.OneToOneField(User, blank=True, null=True, on_delete=models.CASCADE,)
  slug = models.SlugField(max_length=128, blank=True, unique=True, default=uuid.uuid4,)
  birthdate = models.DateField( null=True, default=timezone.now,)
  gender = models.CharField(max_length=16, choices=GENDER_CHOICES, null=True, default=MALE,)
  bio = RichTextField(blank=True)
  phone_number = models.CharField(max_length=24, null=True,)
  facebook_profile = models.URLField(max_length=255,)
  linkedin_profile = models.URLField(max_length=255,)
  picture = models.ImageField(upload_to=user_directory_path,)
  header_image = models.ImageField(blank=True, upload_to=user_directory_path,)

  def __str__(self):
    
    if self.user is not None:
      return '{}'.format(self.user.username)
    return '{} (not registered)'.format(self.user)

  def get_absolute_url(self):
    return reverse('myprofile', args=[str(self.slug)])

@receiver(post_save, sender=User) #ketika user dibuat otomatis membuat profile
def create_user_profile(sender, instance, created, **kwargs):
  if created:
    Profile.objects.create(user=instance)


"""
ECOSYSTEM ENTITIES
"""

"""
MENTOR
"""

class Mentor(models.Model):
  mentor = models.OneToOneField(User,blank=True, null=True, on_delete=models.CASCADE)
  slug  = models.SlugField(max_length=128, blank=True, null=True, unique=True)

  def __str__(self):
    return '{}'.format(self.mentor.username)

  # def get_absolute_url(self):
    # return reverse('mentor-detail', kwargs={'slug':self.slug})
    # return reverse('mentor-detail', args=[str(self.user.slug)])
    # return reverse('mentor-detail', kwargs={'pk':self.pk})

  def communication_avg(self):
    return FeedbackForMentor.objects.filter(mentoring__mentor=self).aggregate(Avg('communication_grade'))

  def relevance_avg(self):
    return FeedbackForMentor.objects.filter(mentoring__mentor=self).aggregate(Avg('relevance_grade'))

  def helpfullness_avg(self):
    return FeedbackForMentor.objects.filter(mentoring__mentor=self).aggregate(Avg('helpfullness_grade'))


"""
STARTUP
"""

def startup_directory_path(instance, filename):
  # file will be uploaded to MEDIA_ROOT/<name>/<filename>
  return 'startup/{0}/{1}'.format(instance.name,filename)

class Startup(models.Model):
  name = models.CharField(max_length=255,)
  user = models.ForeignKey(User,blank=True, null=True, on_delete=models.CASCADE)
  slug = models.SlugField(max_length=128, blank=True, unique=True)
  picture = models.ImageField(blank=True, upload_to=startup_directory_path,)
  header_image = models.ImageField(blank=True, upload_to=startup_directory_path,)
  description = RichTextField()
  vision = RichTextField()
  mission = RichTextField()

  def save(self, *args, **kwargs):
    if self.slug is '' or self.slug is None:
      self.slug = unique_slug_generator_byfield(self, 'name')
    super().save(*args, **kwargs)

  def get_absolute_url(self):
    return reverse('startup-detail', args=[str(self.slug)])

  def __str__(self):
    return self.name

CEO = 'CEO'
CTO = 'CTO'
CFO = 'CFO'
CMO = 'CMO'
WPP = 'WPP'
COO = 'COO'
Member = 'Member'
POSITION_CHOICES = (
  (CEO, 'CEO'),
  (CTO, 'CTO'),
  (CFO, 'CFO'),
  (CMO, 'CMO'),
  (WPP, 'WPP'),
  (COO, 'COO'),
  (Member, 'Member'),  
)

class StartupMember(models.Model):
  member = models.ForeignKey(User, on_delete=models.CASCADE)
  startup = models.ForeignKey(Startup, related_name='startup', on_delete=models.CASCADE)
  position = models.CharField(max_length=16, choices=POSITION_CHOICES, null=True, default=Member, blank=True)

  def __str__(self):
    if self.member is not None:
      return '{}'.format(self.member)
    return '{} (not registered)'.format(self.member)

@receiver(post_save, sender=Startup) #ketika startup dibuat otomatis membuat member
def create_user_member(sender, instance, created, **kwargs):
  if created:
    StartupMember.objects.create(member=instance.user, startup=instance)

@receiver(user_signed_up)
def accept_invite(sender, request, user, **kwargs):
    addresses = EmailAddress.objects.filter(user=user, verified=True) \
                                    .values_list('email', flat=True)

    invites = Invitation.objects.filter(email__in=addresses)
    if invites:
        invites.update(accepted=True)
        for invite in invites:
            invite_accepted.send(sender=Invitation, email=invite.email)
            a = invite.inviter
            b = a.startup_set.first()  
            StartupMember.objects.create(member=user, startup=b)
  
class RequestStartupMember(models.Model):
  user_rq = models.ForeignKey(User, related_name='+', on_delete=models.CASCADE)
  startup_rq = models.ForeignKey(Startup, related_name='member_request', on_delete=models.CASCADE)
  approval = models.BooleanField(default=False)
  created = models.DateTimeField(auto_now_add=True)

  def __str__(self):
    return '{}'.format(self.user_rq)

class QualificationAsk(models.Model):
  pertanyaan = models.CharField(max_length=255)


  def __str__(self):
    return '{}'.format(self.pertanyaan)

class QualificationOption(models.Model):
  mentor_pertanyaan = models.ForeignKey(QualificationAsk, related_name='mentor_pertanyaan', on_delete=models.CASCADE)
  opsi = models.CharField(max_length=255, null=True, blank=True)
  kunci = models.BooleanField(default=False)  

  def __str__(self):
    return '{}'.format(self.opsi)

class QualificationAnswer(models.Model):
  user_mentor = models.ForeignKey(User, related_name='user_mentor', on_delete=models.CASCADE)
  jawaban = models.ForeignKey(QualificationOption, related_name='jawaban', on_delete=models.CASCADE)

  def __str__(self):
    return '{}'.format(self.user_mentor)



