from django.shortcuts import render, get_object_or_404, redirect
from .models import *
from .forms import *
from django.views.generic import TemplateView, ListView, DetailView
from django.views.generic.edit import FormView, UpdateView, CreateView, DeleteView
from django.http import HttpResponseRedirect
from django.urls import reverse,reverse_lazy,resolve
from django.forms.formsets import formset_factory
from django.views import View
from django.utils.deprecation import MiddlewareMixin
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator


class CreateProfile(UpdateView):
    template_name = '../templates/account/profile_create.html'
    model = Profile
    form_class = ProfileForm

    def get_success_url(self, *args, **kwargs):
        return reverse_lazy('profile_create_experience', args=[str(self.request.user.profile.slug)])

class CreateProfileExperience(CreateView):
    template_name= '../templates/account/profile_experience.html'
    model = ExperienceData
    form_class = ExperienceForm

    def form_valid(self, form):
        form.instance.profile = self.request.user.profile
        return super().form_valid(form)

    def get_success_url(self, *args, **kwargs):
        return reverse_lazy('profile_create_education', args=[str(self.request.user.profile.slug)])

class CreateProfileEducation(CreateView):
    template_name = '../templates/account/profile_education.html'
    model = EducationData
    form_class = EducationForm

    def form_valid(self, form):
        form.instance.profile = self.request.user.profile
        return super().form_valid(form)

    def get_success_url(self, *args, **kwargs):
        return reverse_lazy('profile_create_skill', args=[str(self.request.user.profile.slug)])

class CreateProfileSkill(CreateView):
    template_name = '../templates/account/profile_skill.html'
    model = SkillData
    form_class = SkillForm

    def form_valid(self, form):
        form.instance.profile = self.request.user.profile
        return super().form_valid(form)
    
    def get_success_url(self, *args, **kwargs):
        return reverse_lazy('role-entities')

class ProfileView(TemplateView):
    template_name = '../templates/account/profile.html'
    model = Profile

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        context['profile_data'] = Profile.objects.get(slug=self.kwargs['slug'])
        context['request_startup'] = RequestStartupMember.objects.filter(user_rq__profile__slug=self.kwargs['slug'],approval=False)

        return context

class ProfileUpdate(UpdateView):
    template_name = '../templates/account/profile_update.html'
    model = Profile
    form_class = ProfileForm



class ProfileUserUpdate(UpdateView):
    template_name = '../templates/account/profile_updated.html'
    model = User
    form_class = ProfileUserForm

    def get_object(self, queryset=None):
        user = self.request.user
        
        return user

    def get_success_url(self):
        return reverse('myprofile',args=[str(self.request.user.profile.slug)])



# class ProfileExperienceAdd(View):
#  def post(self, request, *args, **kwargs):

#         if request.method == "POST":
#             experience_form = ExperienceForm(request.POST)

#             if experience_form.is_valid():
#                 title = experience_form.cleaned_data['title']
#         else:
#             experience_form = ExperienceForm()
        
#         return redirect('profile-update-profile')

class ProfileExperienceAdd(CreateView):
    template_name= '../templates/account/profile_experience.html'
    model = ExperienceData
    form_class = ExperienceForm

    def form_valid(self, form):
        form.instance.profile = self.request.user.profile
        return super().form_valid(form)

    def get_success_url(self, *args, **kwargs):
        return reverse_lazy('myprofile', args=[str(self.request.user.profile.slug)])

class ProfileExperienceUpdate(UpdateView):
    template_name = '../templates/account/profile_experience_update.html'
    model = ExperienceData
    form_class = ExperienceForm

class ProfileExperienceDelete(DeleteView):
    template_name = '../templates/account/experiencedata_confirm_delete.html'
    model = ExperienceData

    def get_success_url(self, *args, **kwargs):
        return reverse_lazy('myprofile', args=[str(self.request.user.profile.slug)])

class ProfileEducationAdd(CreateView):
    template_name = '../templates/account/profile_education.html'
    model = EducationData
    form_class = EducationForm

    def form_valid(self, form):
        form.instance.profile = self.request.user.profile
        return super().form_valid(form)

    def get_success_url(self, *args, **kwargs):
        return reverse_lazy('myprofile', args=[str(self.request.user.profile.slug)])

class ProfileEducationUpdate(UpdateView):
    template_name = '../templates/account/profile_experience_update.html'
    model = EducationData
    form_class = EducationForm

class ProfileEducationDelete(DeleteView):
    template_name = '../templates/account/profile_education_confirm_delete.html'
    model = EducationData

    def get_success_url(self, *args, **kwargs):
        return reverse_lazy('myprofile', args=[str(self.request.user.profile.slug)])

class ProfileSkillAdd(CreateView):
    template_name = '../templates/account/profile_skill.html'
    model = SkillData
    form_class = SkillForm

    def form_valid(self, form):
        form.instance.profile = self.request.user.profile
        return super().form_valid(form)

class ProfileSkillUpdate(UpdateView):
    template_name = '../templates/account/profile_skill_update.html'
    model = SkillData
    form_class = SkillForm

class ProfileSkillDelete(DeleteView):
    template_name = '../templates/account/profile_skill_confirm_delete.html'
    model = SkillData

    def get_success_url(self, *args, **kwargs):
        return reverse_lazy('myprofile', args=[str(self.request.user.profile.slug)])

class MentorQualification(ListView):
    template_name = '../templates/mentor/mentor_qualification.html'
    model = QualificationAsk

    def get_context_data(self, *args, **kwargs):
        context = super(MentorQualification, self).get_context_data(**kwargs)
        context['qualification'] = QualificationAsk.objects.all()
        return context

class MentorQualificationAnswer(View):
    def post(self, request, *args, **kwargs):

        if request.method == "POST":
            
            form = request.POST
            print(form)

            for key, value, in form.items():
                print(key, value)

                if key != 'csrfmiddlewaretoken':

                    user = self.request.user
                    jawab = QualificationAnswer.objects.create(jawaban_id=value, user_mentor_id=user.id)
                    
                    print(jawab)

# jawaban = QualificationAnswer.objects.filter(user_mentor=user)
# print(jawaban)
# jawab3 = jawaban.first().jawaban.kunci
# print(jawab3)


                    # #value: variable opsi
                    print(value)

                    # #ambil opsi
                    opsi = QualificationOption.objects.get(id=value)
                    jawaban = opsi.kunci

                    if jawaban == True:
                        create_mentor, created = Mentor.objects.get_or_create(mentor=user)
                        
                    else:
                        print('gagal')


# break

# kunci = QualificationOption.objects.all().filter(kunci=True)
# print(kunci)
# print(kunci.values('id'))



# if jawab3 == True:
# kunci == True
# print(kunci)

# if kunci == True:
# create_mentor,created = Mentor.objects.get_or_create(mentor=user)
# else:
# pass
# else:
# pass


# print(jawab.values('jawaban_id'))

# if jawab or kunci:
# print('oke')
# create_mentor,created = Mentor.objects.get_or_create(mentor=user)
# create_mentor.save()
# else:
# print('gagal')


#get value from dictionary
# tes = form.get('Apakah kamu founder?')
# print(tes)
            
        
        return redirect('qualification_status_mentor')

class MentorQualificationStatus(TemplateView):
    template_name = '../templates/mentor/status_mentor.html'
    model = Mentor

    def get_context_data(self, *agrs, **kwargs):
        context = super(MentorQualificationStatus, self).get_context_data(**kwargs)
        context['data_mentor'] = Mentor.objects.filter(mentor=self.request.user)

        # if context['data_mentor'] > 1 :
        #     context['mentor']
        # else:
        #     pass

        return context
