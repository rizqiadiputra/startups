from django.contrib import admin
from .models import *
# Register your models here.

class SchedulingAdmin(admin.ModelAdmin):
    list_display = ('program','description','mentor','datetime','location')
admin.site.register(Scheduling, SchedulingAdmin)

class MentoringAdmin(admin.ModelAdmin):
    list_display = ('datetime','title','mentor','startup',)
admin.site.register(Mentoring, MentoringAdmin)

class StartupProgressAdmin(admin.ModelAdmin):
    list_display = ('startup','week','activation','acquisition','revenue','referral','retention')
admin.site.register(StartupProgress, StartupProgressAdmin)

class FeedbackForMentorAdmin(admin.ModelAdmin):
    list_display = ('mentoring','communication_grade','helpfullness_grade','relevance_grade')
admin.site.register(FeedbackForMentor,FeedbackForMentorAdmin)

class FeedbackForStartupAdmin(admin.ModelAdmin):
    list_display = ('mentoring','communication_grade','attitude_grade','perseverance_grade')    
admin.site.register(FeedbackForStartup,FeedbackForStartupAdmin)

class TaskAdmin(admin.ModelAdmin):
    list_display = ('mentoring','task_for_startup','startup_assignment_report','mentor_assignment_feedback')
admin.site.register(Task, TaskAdmin)

admin.site_header = "Startup Success"