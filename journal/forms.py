from django import forms
from ecosystem.models import Startup, Mentor
from .models import *
from django.forms import ModelForm

class MentorForm(forms.ModelForm):    
    class Meta:
        model = Mentor
        fields = '__all__'
        exclude = ()

class StartupForm(forms.Form):
    name = forms.CharField()

    def data_bio(self):
        pass

class NewStartupForm(forms.ModelForm):
    class Meta:
        model = Startup
        fields = ['name','slug','description','vision','mission','picture','header_image']

class StartupsForm(ModelForm):  
    class Meta:
        model = Startup
        fields = '__all__'

class EmailForm(forms.ModelForm):
    class Meta:
        # model = Profile
        fields = ('email',)

class EmailsForm(forms.Form):
    email = forms.CharField(max_length=255)

class StartupProgressForm(forms.ModelForm):
    class Meta:
        model = StartupProgress
        fields = ['mentoring','week', 'acquisition','picture_acquisition','url_acquisition','activation','picture_activation','url_activation','retention','picture_retention','url_retention','revenue','picture_revenue','url_revenue','referral','picture_referral','url_referral','note','datetime',]

class MentorMentoringForm(forms.ModelForm):
    class Meta:
        model = Mentoring
        fields = ['program','title','mentor','startup',]
        exclude = ()

class MentorMentoringNoteForm(forms.ModelForm):
    class Meta:
        model = Mentoring
        fields = ['mentor_question','mentor_answer','mentor_notes',]
        exclude = ()

class StartupMentoringForm(forms.ModelForm):
    class Meta:
        model = Mentoring
        fields = ['startup_answer','startup_question','startup_notes',]
        exclude = ()

class MentorTaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['mentoring','mentor','startup','task_for_startup',]
        exclude = ()

# mentoring = models.ForeignKey('journal.Mentoring', on_delete=models.CASCADE)
#   mentor = models.ForeignKey('ecosystem.Mentor', on_delete=models.CASCADE)
#   startup = models.ForeignKey('ecosystem.Startup', on_delete=models.CASCADE)
#   task_for_startup = RichTextField(blank=True)
#   startup_assignment_report = RichTextField(blank=True)
#   mentor_assignment_feedback = RichTextField(blank=True)

class StartupAddTaskMentoringForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['startup_assignment_report',]