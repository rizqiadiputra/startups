import json
import datetime
import uuid
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from django.utils.text import slugify
from django.db import models
from django.db.models.signals import m2m_changed
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.auth.models import User
from django.dispatch import receiver
from allauth.account.signals import user_signed_up
from ckeditor.fields import RichTextField
from util.randomizer import unique_slug_generator, unique_slug_generator_byfield
from config.models import *
from django.urls import reverse


class Scheduling(models.Model):
  program = models.ForeignKey('config.Program', on_delete=models.CASCADE)
  description = RichTextField(blank=True)
  mentor = models.ForeignKey('ecosystem.Mentor', on_delete=models.CASCADE)
  startup = models.ManyToManyField('ecosystem.Startup')
  venue  = models.CharField(max_length=255, default='Common Room')
  capacity = models.CharField(max_length=255, blank=True)
  datetime = models.DateTimeField(default=timezone.now)
  location = models.CharField(max_length=255, default='EDS')
  status_mentor = models.BooleanField(default=False)
  status_startup = models.BooleanField(default=False)
  created = models.DateTimeField(auto_now_add=True)

  def __str__(self):
    return self.program.name

class Mentoring(models.Model):
  id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
  program = models.ForeignKey('config.Program', on_delete=models.CASCADE)
  datetime = models.DateTimeField(default=timezone.now)
  title = models.CharField(max_length=255)
  mentor = models.ForeignKey('ecosystem.Mentor', on_delete=models.CASCADE)
  startup = models.ForeignKey('ecosystem.Startup', on_delete=models.CASCADE, blank=True)
  # mentor_attendance = models.BooleanField(default=True)
  # founder_attendance = models.ManyToManyField('ecosystem.StartupMember', related_name='founder_attendance', blank=True)
  mentor_question = RichTextField(blank=True)
  startup_answer = RichTextField(blank=True)
  startup_question = RichTextField(blank=True)
  mentor_answer = RichTextField(blank=True)
  mentor_notes = RichTextField(blank=True)
  startup_notes = RichTextField(blank=True)
  location = models.CharField(max_length=255, default='EDS')

  def get_absolute_url(self):
    return reverse('mentoring-feedback', args=[str(self.id)])

  def __str__(self):
      return self.title

class Task(models.Model):
  mentoring = models.ForeignKey('journal.Mentoring', on_delete=models.CASCADE)
  mentor = models.ForeignKey('ecosystem.Mentor', on_delete=models.CASCADE)
  startup = models.ForeignKey('ecosystem.Startup', on_delete=models.CASCADE)
  task_for_startup = RichTextField(blank=True)
  startup_assignment_report = RichTextField(blank=True)
  mentor_assignment_feedback = RichTextField(blank=True)
  
  def __str__(self):
    return self.task_for_startup


class FeedbackForMentor(models.Model):
  mentoring = models.ForeignKey('journal.Mentoring', on_delete=models.CASCADE)
  communication_grade = models.PositiveIntegerField()
  relevance_grade = models.PositiveIntegerField()
  helpfullness_grade = models.PositiveIntegerField()
  note = RichTextField(blank=True)

  def get_absolute_url(self):
    return reverse('mentoring-feedback', args=[str(self.mentoring.id)])

  # def __str__(self):
  #   return self.note

class FeedbackForStartup(models.Model):
  mentoring = models.ForeignKey('journal.Mentoring', on_delete=models.CASCADE)
  communication_grade = models.PositiveIntegerField()
  attitude_grade = models.PositiveIntegerField()
  perseverance_grade = models.PositiveIntegerField()
  note = RichTextField(blank=True)

  def get_absolute_url(self):
    return reverse('mentoring-feedback', args=[str(self.mentoring.id)])


def startupprogress_directory_path(instance, filename):
  return 'startupprogress/{0}/{1}'.format(instance.startup.name,filename)

class StartupProgress(models.Model):
  startup = models.ForeignKey('ecosystem.Startup', on_delete=models.CASCADE)
  mentoring = models.ForeignKey('journal.Mentoring', on_delete=models.CASCADE)
  week = models.PositiveIntegerField()
  acquisition = models.PositiveIntegerField()
  picture_acquisition = models.ImageField(blank=True, upload_to=startupprogress_directory_path,)
  url_acquisition = models.URLField(max_length=255, blank=True)
  activation = models.PositiveIntegerField()
  picture_activation = models.ImageField(blank=True, upload_to=startupprogress_directory_path,)
  url_activation = models.URLField(max_length=255, blank=True)
  retention = models.PositiveIntegerField()
  picture_retention = models.ImageField(blank=True, upload_to=startupprogress_directory_path,)
  url_retention = models.URLField(max_length=255, blank=True)
  revenue = models.PositiveIntegerField()
  picture_revenue = models.ImageField(blank=True, upload_to=startupprogress_directory_path,)
  url_revenue = models.URLField(max_length=255, blank=True)  
  referral = models.PositiveIntegerField()
  picture_referral = models.ImageField(blank=True, upload_to=startupprogress_directory_path,)
  url_referral = models.URLField(max_length=255, blank=True)
  note = RichTextField(blank=True)
  datetime = models.DateTimeField(default=timezone.now)

  def get_absolute_url(self):
    return reverse('startup-detail', args=[str(self.startup.slug)])

