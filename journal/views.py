from django.shortcuts import render, get_object_or_404, redirect
from ecosystem.models import *
from journal.models import *
from django.views.generic import TemplateView, DetailView, ListView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .forms import *
from django.views.generic.edit import FormView, CreateView, DeleteView, UpdateView
from django.urls import reverse_lazy
from django.db.models.functions import TruncMonth as Month
from django.db.models import Sum, Q, Count
from django_weasyprint import WeasyTemplateResponseMixin
from django.conf import settings
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views import View
from django.http import Http404
from invitations.models import Invitation
from datetime import timedelta, datetime
from django.utils import timezone
import datetime

@method_decorator(login_required, name='dispatch')
class RoleEntitiesView(TemplateView):
    template_name = '../templates/entities_role.html'

@method_decorator(login_required, name='dispatch')
class MentorAdd(CreateView):
    template_name = '../templates/mentor/mentor_create.html'
    model = Mentor
    form_class = MentorForm

    def form_valid(self, form):

        user = self.request.user
        form.instance.user = user                    
    
        return super().form_valid(form)

    def get_success_url(self, *args, **kwargs):
        return reverse_lazy('qualification_mentor')

@method_decorator(login_required, name='dispatch')
class RoleStartupView(ListView):
    template_name = '../templates/startup/startup_new.html'
    model = Startup

    def get_context_data(self, **kwargs):
        context = super(RoleStartupView, self).get_context_data(**kwargs)
        context['startup'] = Startup.objects.all()

        return context

@method_decorator(login_required, name='dispatch')
class StartupAdd(CreateView):
    template_name = '../templates/startup/startup_create.html'
    model = Startup
    form_class = NewStartupForm

    def form_valid(self, form):

        user = self.request.user
        form.instance.user = user                    
    
        return super().form_valid(form)

@method_decorator(login_required, name='dispatch')
class RequestJoinView(View):
    def get(self, request, *args, **kwargs):
        startup = Startup.objects.get(slug=self.kwargs['slug'])
        query, created = RequestStartupMember.objects.get_or_create(user_rq=self.request.user, startup_rq=startup)
        query.save()

        return redirect('role-startup')

@method_decorator(login_required, name='dispatch')
class ApprovalView(View):
    def get(self, request, *args, **kwargs):
        startup = RequestStartupMember.objects.get(startup_rq__slug=self.kwargs['slug'], id = self.kwargs['pk'])
        member, created = StartupMember.objects.get_or_create(member=startup.user_rq, startup=startup.startup_rq)
        startup.approval = True
        startup.save()

        return redirect('startup-detail',slug=self.kwargs['slug'])

@method_decorator(login_required, name='dispatch')
class MemberListView(ListView):
    template_name = '../templates/startup/member_create.html'
    model = User
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super(MemberListView, self).get_context_data(**kwargs)
        context['newmember'] = User.objects.filter(startupmember__isnull=True, mentor__isnull=True).distinct()
    
        return context

    def get_queryset(self):
        result = super(MemberListView, self).get_queryset()
        query = self.request.GET.get('s')

        if query:
            result = result.filter(username__icontains=query)

        return result

@method_decorator(login_required, name='dispatch')
class RequestInviteMemberView(View):
    def get(self, request, *args, **kwargs):
        qs = User.objects.filter(startupmember__isnull=True, mentor__isnull=True).distinct().get(id=self.kwargs['pk'])
        mystartup = User.objects.get(username=self.request.user)
        startupku = mystartup.startup_set.all()
       
        for ss in startupku:
            startupq = ss
        startup=startupq
       
        startuprq, created = RequestStartupMember.objects.get_or_create(startup_rq=startup, user_rq = qs)
        startuprq.save()

        return redirect('member-invite')

@method_decorator(login_required, name='dispatch')
class ApprovalMemberView(View):
    def get(self, request, *args, **kwargs):
        startup = RequestStartupMember.objects.get(startup_rq=self.kwargs['pk'], user_rq__profile__slug = self.kwargs['slug'])
        member, created = StartupMember.objects.get_or_create(member=startup.user_rq, startup=startup.startup_rq)
        startup.approval = True
        startup.save()

        return redirect('myprofile',slug=self.kwargs['slug'])

@method_decorator(login_required, name='dispatch')
class InviteEmailMemberView(View):
    def post(self, request, *args, **kwargs):

        if request.method == "POST":
            MyEmailForm = EmailsForm(request.POST)
            print(MyEmailForm)

            if MyEmailForm.is_valid():
                email = MyEmailForm.cleaned_data['email']
                print(email)
                invite = Invitation.create(email, inviter=self.request.user)
                invite.send_invitation(request)
        else:
            MyEmailForm = EmailsForm()
        
        return redirect('member-invite')


@method_decorator(login_required, name='dispatch')
class StartupListView(ListView):
# class StartupListView(ListView, FormView):

    template_name = '../templates/startup/startup.html'
    model = Startup
    # form_class = StartupForm
    # success_url = '/thanks/'
    paginate_by = 8

    def get_context_data(self, **kwargs):
        context = super(StartupListView, self).get_context_data(**kwargs)
        # context['startupData'] = Startup.objects.all()
        # context['startupProgress'] = StartupProgress.objects.all().order_by('-datetime')
        # context['range'] = range(context["paginator"].num_pages)

        best_data =  StartupProgress.objects.select_related('startup').values('startup__name', 'startup__picture', 'startup__header_image', 'activation','acquisition','revenue','week').order_by('-revenue')
        if len(best_data) < 1:
            context['bestStartup'] = []
        else:
            context['bestStartup'] = best_data[0]

        return context
    
    # def form_valid(self, form):
    #     form.data_bio()
    #     return super().form_valid(form)

    def get_queryset(self):
        result = super(StartupListView, self).get_queryset().annotate(total_revenue=Sum('startupprogress__revenue')).annotate(total_acquisition=Sum('startupprogress__acquisition')).annotate(total_activation=Sum('startupprogress__activation')).order_by('-total_revenue')

        query = self.request.GET.get('s')
        if query:
            result = result.filter( name__icontains=query)
        return result

@method_decorator(login_required, name='dispatch')
class StartupDetailView(DetailView):
    template_name = '../templates/startup/detail.html'
    # template_name = '../templates/startup/generate-pdf.html'
    model = Startup
    # slug_field = "slug"

    def get_context_data(self, **kwargs):
        revenueData = []
        retentionData = []
        referralData = []
        activationData = []
        acquisitionData = []
        categoriesData = []
       
        startupProgress =  StartupProgress.objects.filter(startup__slug=self.kwargs['slug']).order_by('-datetime')

        for progress in startupProgress.reverse():
            revenueData.append(progress.revenue)
            retentionData.append(progress.retention)
            referralData.append(progress.referral)
            activationData.append(progress.activation)
            acquisitionData.append(progress.acquisition)
            categoriesData.append(progress.week)

        progressChange = {}
       
        if startupProgress.count() > 1:
            progressChange['revenue'] = 100 * (startupProgress[0].revenue - startupProgress[1].revenue)/(startupProgress[1].revenue)
            progressChange['retention'] = 100 * (startupProgress[0].retention - startupProgress[1].retention)/(startupProgress[1].retention)
            progressChange['referral'] = 100 * (startupProgress[0].referral - startupProgress[1].referral)/(startupProgress[1].referral)
            progressChange['activation'] = 100 * (startupProgress[0].activation - startupProgress[1].activation)/(startupProgress[1].activation)
            progressChange['acquisition'] = 100 * (startupProgress[0].acquisition - startupProgress[1].acquisition)/(startupProgress[1].acquisition)
        else:
            progressChange['revenue'] = 0
            progressChange['retention'] = 0
            progressChange['referral'] = 0
            progressChange['activation'] = 0
            progressChange['acquisition'] = 0

        context = super(StartupDetailView, self).get_context_data(**kwargs)
        context['startupProgress'] = StartupProgress.objects.filter(startup__slug=self.kwargs['slug']).order_by('-datetime')
        context['startupProgressMonthly'] = StartupProgress.objects.filter(startup__slug=self.kwargs['slug']).annotate(month=Month('datetime')).values('month').annotate(acquisition_sum=Sum('acquisition'), activation_sum=Sum('activation'), retention_sum=Sum('retention'), revenue_sum = Sum('revenue'), referral_sum=Sum('referral'))
        context['startupMentoring'] = Startup.objects.get(slug=self.kwargs['slug']).mentoring_set.all()[:2]        
        context['matchMentor'] = Mentor.objects.filter(mentoring__startup__slug=self.kwargs['slug']).annotate(Sum('mentoring__feedbackformentor__helpfullness_grade'))
        context['feedbackStartup'] = FeedbackForStartup.objects.filter(mentoring__startup__name = self.kwargs['slug'])
        context['revenueData'] = revenueData
        context['retentionData'] = retentionData
        context['referralData'] = referralData
        context['activationData'] = activationData
        context['acquisitionData'] = acquisitionData
        context['categoriesData'] = categoriesData
        context['progressChange'] = progressChange
        context['team'] = Startup.objects.filter(slug=self.kwargs['slug'])
        context['startup_schedule'] = Startup.objects.get(slug=self.kwargs['slug']).scheduling_set.all() 
        context['startup_task'] = Startup.objects.get(slug=self.kwargs['slug']).task_set.all()

        # context['team'] = Startup.objects.filter(slug=self.kwargs['slug']).values('startup__member__profile__picture','startup__member__username', 'startup__position')
        # context['team'] = Startup.objects.filter(slug=self.kwargs['slug']).values('startup__member__profile__picture','startup__member__username', 'startup__position','startup__member__profile__skills__name')

        context['request_member'] = RequestStartupMember.objects.exclude(startup_rq__slug=self.kwargs['slug'], approval=True)

        userku = self.request.user
        staff = userku.is_staff
        slug = self.kwargs['slug']
        startup_member = self.request.user.startupmember_set.filter(startup__slug=slug)
        edit = startup_member or staff==True

        if edit:
            context['edit'] = edit

        datenow = datetime.datetime.today()
        # print(datenow)

        startdate = datetime.datetime.today() - datetime.timedelta(days=datetime.datetime.today().isoweekday() % 7)
        # print(startdate)
        enddate = startdate + datetime.timedelta(days=6)
        # print(enddate)

    

        dateprogress = StartupProgress.objects.filter(startup__slug=slug,datetime__gte=startdate, datetime__lte=enddate)
        print(dateprogress)

        cekk = len(dateprogress) < 1
        print(cekk)

        if cekk:
            context['cekk'] = cekk

        if dateprogress:
            context['dateprogress'] = dateprogress
        # elif cekk == True:
            # context['belumisi'] = cekk

        
        return context

@method_decorator(login_required, name='dispatch')
class GenerateStartupPdf(WeasyTemplateResponseMixin, DetailView):
    template_name = '../templates/startup/generate-pdf.html'
    model = Startup
    slug_field = "slug"

    pdf_stylesheets = [
        settings.STATIC_ROOT + '/css/dashboard.css',
        settings.STATIC_ROOT + '/css/tabler.css',
    ]

    def get_context_data(self, **kwargs):
        context = super(GenerateStartupPdf, self).get_context_data(**kwargs)
        context['startupProgress'] = StartupProgress.objects.filter(startup__slug=self.kwargs['slug']).order_by('-datetime')
        context['startupProgressMonthly'] = StartupProgress.objects.filter(startup__slug=self.kwargs['slug']).annotate(month=Month('datetime')).values('month').annotate(acquisition_sum=Sum('acquisition'), activation_sum=Sum('activation'), retention_sum=Sum('retention'), revenue_sum = Sum('revenue'), referral_sum=Sum('referral'))
        
        return context

@method_decorator(login_required, name='dispatch')
class StartupMentoringDetailView(DetailView):
    template_name = '../templates/startup/mentoring-detail.html'
    model = Startup
    slug_field = 'slug'

    def get_context_data(self, **kwargs):
        context = super(StartupMentoringDetailView, self).get_context_data(**kwargs)
        context['dataMentoring'] = Mentoring.objects.get(id = self.kwargs['pk'])
        return context

# class StartupMentoringAddTask(UpdateView):
#     template_name = '../templates/startup/mentoring-task.html'
#     model = Startup
#     slug_field = 'slug'

#     def get_context_data(self, **kwargs):
#         cont

@method_decorator(login_required, name='dispatch')
class StartupMentoringAddTask(UpdateView):
    template_name = '../templates/startup/startup_add_task_mentoring.html'
    model = Task
    form_class = StartupAddTaskMentoringForm

    # def get_context_data(self, **kwargs):
    #     context = super(StartupMentoringAddTask, self).get_context_data(**kwargs)
    #     context['startupTaskMentoring'] = Startup.objects.get(slug=self.kwargs['slug']).mentoring_set.all()[:2]

    #     return context
    def get_success_url(self, *args, **kwargs):

        return reverse_lazy('startup-list',)

@method_decorator(login_required, name='dispatch')
class StartupScheduleDetailView(DetailView):
    template_name = '../templates/startup/schedule_detail.html'
    model = Startup
    slug_field = 'slug'

    def get_context_data(self, **kwargs):
        context = super(StartupScheduleDetailView, self).get_context_data(**kwargs)
        # context['data_schedule'] = Scheduling.objects.get(id = self.kwargs['pk'])
        context['data_schedule'] = Startup.objects.get(slug=self.kwargs['slug']).scheduling_set.all() 

        return context



    # def post(self, request):
    #     name = request.POST.get("name")
    #     slug = request.POST.get("slug")
    #     description = request.POST.get("description")
    #     vision = request.POST.get("vision")
    #     mission = request.POST.get("mission")
    #     picture = request.POST.get("picture")
    #     header_image = request.POST.get("header_image")
    #     user = request.user

    #     startup = Startup(
    #         name=name,
    #         user=user,
    #         slug=slug,
    #         description=description,
    #         vision=vision,
    #         mission=mission,
    #         picture=picture,
    #         header_image=header_image
    #         )

        # startup.save()
        # return HttpResponseRedirect(reverse('startup-detail', slug=slug))
        # return redirect('startup-detail', slug=slug)

@method_decorator(login_required, name='dispatch')
class StartupUpdate(UpdateView):
    template_name = '../templates/startup/startup_update.html'
    model = Startup
    fields = ['name','slug','description','vision','mission','picture','header_image']

@method_decorator(login_required, name='dispatch')
class StartupDelete(DeleteView):
    model = Startup
    success_url = reverse_lazy('startup-list')

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

@method_decorator(login_required, name='dispatch')
class StartupUpdateMentoring(UpdateView):
    template_name = '../templates/startup/startup_update_mentoring.html'
    model = Mentoring
    form_class = StartupMentoringForm

    def get_context_data(self, **kwargs):
        context = super(StartupUpdateMentoring, self).get_context_data(**kwargs)
        context['startupMentorings'] = Startup.objects.get(slug=self.kwargs['slug']).mentoring_set.all()[:2]

        return context

@method_decorator(login_required, name='dispatch')
class StartupAddProgress(CreateView):
    template_name = '../templates/startup/startup_add_progress.html'
    model = StartupProgress
    form_class = StartupProgressForm

    def form_valid(self, form):

        # slug = self.kwargs['slug']
        user = self.request.user
        member = user.startupmember_set.get(member=user)
        startup = member.startup
        print(startup)
        # form.instance.startup = self.request.user.startupprogress_set.get(startup=startup)
        form.instance.startup = startup                    
    
        return super().form_valid(form)

@method_decorator(login_required, name='dispatch')
class StartupDetailProgress(DetailView):
    template_name = '../templates/startup/startup_detail_progress.html'
    model = StartupProgress

    def get_context_data(self, **kwargs):
        context = super(StartupDetailProgress, self).get_context_data(**kwargs)
        context['startup'] = self.request.user.startup.slug

@method_decorator(login_required, name='dispatch')
class StartupUpdateProgress(UpdateView):
    template_name = '../templates/startup/startup_update_progress.html'
    model = StartupProgress
    form_class = StartupProgressForm

class StartupDeleteProgress(DeleteView):
    template_name = '../templates/startup/startup_progress_confirm_delete.html'
    model = StartupProgress

    def get_success_url(self, *args, **kwargs):
        startup = self.request.user.startup_set.all()
        for startups in startup:
            pass
        return reverse_lazy('startup-detail', args=[str(startups.slug)])

class StartupFeedbackForMentor(CreateView):
    template_name = '../templates/mentoring/feedback_for_mentor.html'
    model = FeedbackForMentor
    fields = '__all__'


"""
MENTOR
"""
@method_decorator(login_required, name='dispatch')
class MentorListView(ListView):
    template_name = '../templates/mentor/mentor.html' #default:  <app_label>/<model_name>_list.html
    model = Mentor
    paginate_by = 8
    context_object_name = 'mentors' #default: object_list
    # queryset = Mentor.objects.all() #default: Model.objects.all()

    def get_context_data(self, **kwargs):
        context = super(MentorListView, self).get_context_data(**kwargs)
        best_mentor = FeedbackForMentor.objects.select_related('mentoring').order_by('-helpfullness_grade')
    #    best_mentor = Mentor.objects 
        if len(best_mentor) < 1:
            context['bestMentor'] = None
        else:
            context['bestMentor'] = best_mentor[0] 

        return context

    def get_queryset(self):
        result = super(MentorListView, self).get_queryset()
        query = self.request.GET.get('s')

        if query:
            result = result.filter( mentor__username__icontains=query)

        return result

@method_decorator(login_required, name='dispatch')
class MentorDetailView(DetailView):
    template_name = '../templates/mentor/detail.html'
    model = Mentor
    # slug_field = 'slug'

    def get_context_data(self, **kwargs):
        context = super(MentorDetailView, self).get_context_data(**kwargs)
        context['mentor_schedule'] = Scheduling.objects.filter(pk=self.kwargs['pk'])
        context['mentor_mentoring'] = Mentoring.objects.filter(mentor__pk=self.kwargs['pk'])
        context['startup_attendance'] = Scheduling.objects.filter(pk=self.kwargs['pk']).filter(status_startup=True).count()
        print(context['startup_attendance'])
        # print(kwargs)
        # context['mentor_schedule'] = Scheduling.objects.filter(mentor__id=self.kwargs['id'])
        # context['mentor_mentoring'] = Mentoring.objects.filter(mentor__slug=self.kwargs['slug'])
        # context['startup_attendance'] = Scheduling.objects.filter(mentor__slug=self.kwargs['slug']).filter(status_startup=True).count()
      


        # userku = self.request.user
        # staff = userku.is_staff
        # slug = self.kwargs['slug']
        # startup_member = self.request.user.startupmember_set.filter(startup__slug=slug)
        # edit = startup_member or staff==True

        # if edit:
        #     context['edit'] = edit

        #ini untuk button edit > dapat edit data
        # context['mentorData'] = Mentor.objects.get(slug=self.kwargs['slug'])
        # context disini utuk menambahkan context model class lain
        # context['feedback'] = FeedbackForMentor.objects.all()

        return context

@method_decorator(login_required, name='dispatch')
class GenerateMentorPdf(WeasyTemplateResponseMixin, DetailView):
    template_name = '../templates/mentor/generate-pdf.html'
    model = Mentor

    pdf_stylesheets = [
        settings.STATIC_ROOT + '/css/tabler.css',
    ]

    def get_context_data(self, **kwargs):
        context = super(GenerateMentorPdf, self).get_context_data(**kwargs)

        return context

@method_decorator(login_required, name='dispatch')
class MentorUpdateSkillsView(UpdateView):
    template_name = '../templates/mentor/mentor-create.html'
    # model = MentorProfile
    model = Mentor

    fields = ['skills',]

@method_decorator(login_required, name='dispatch')
class MentorUpdateView(UpdateView):
    template_name = '../templates/mentor/mentor-update.html'
#     context_object_name = 'update_mentor'
    model = Mentor
    # slug_field = 'slug'   
    fields = ('slug',)

#     def form_valid(self, form):
#         update_mentor = form.save(commit=False)
#         update_mentor.updated_by = self.request.user
#         update_mentor.updated_at = timezone.now()
#         update_mentor.save()

#         return redirect('mentor-detail', slug=update_mentor.kwargs['slug'])


    def get_context_data(self, **kwargs):
        context = super(MentorUpdateView, self).get_context_data(**kwargs)
    #     # print(kwargs)
    #     # context['update_mentor'] = Profile.objects.filter(slug=self.kwargs['slug'])

    #     a = Mentor.objects.all().select_related('mentor')
    #     for aa in a:
    #         print(aa.mentor.profile.bio)

        return context

@method_decorator(login_required, name='dispatch')
class MentorMentoringAdd(CreateView):
    template_name = '../templates/mentor/mentor_add_mentoring.html'
    model = Mentoring
    form_class = MentorMentoringForm

    def get_context_data(self, **kwargs):
        print(kwargs)
        context = super(MentorMentoringAdd, self).get_context_data(**kwargs)
        return context

    # def get_success_url(self, *args, **kwargs):
        # print(self.request.user.mentor.slug)
        # print(kwargs)
        # return reverse_lazy('mentoring_add_note',)
        # return reverse_lazy('myprofile',  args=[str(self.request.user.profile.slug)])
        
class MentorMentoringNote(UpdateView):
    template_name = '../templates/mentor/mentoring/mentor_add_note.html'
    model = Mentoring
    form_class = MentorMentoringNoteForm

    def get_context_data(self, **kwargs):
        context = super(MentorMentoringNote, self).get_context_data(**kwargs)

        return context

    def get_success_url(self, *agrs, **kwargs):
        print(kwargs)
        return reverse_lazy('mentoring-feedback')

class MentorUpdateMentoring(UpdateView):
    template_name = '../templates/mentor/mentor_update_mentoring.html'
    model = Mentoring
    # form_class = MentorMentoringForm

    def get_context_data(self, **kwargs):
        context = super(MentorUpdateMentoring, self).get_context_data(**kwargs)
        context['mentorMentorings'] = Mentor.objects.get(slug=self.kwargs['slug']).mentoring_set.all()

        return context

@method_decorator(login_required, name='dispatch')
class MentorMentoringDetail(DetailView):
    template_name = '../templates/mentor/mentoring-detail.html'
    model = Mentor
    slug_fileld = 'slug'

    def get_context_data(self, **kwargs):
        context = super(MentorMentoringDetail, self).get_context_data(**kwargs)
        context['dataMentor'] = Mentor.objects.filter(slug=self.kwargs['slug'])

        return context

class MentorFeedbackForStartup(CreateView):
    template_name = '../templates/mentoring/feedback_for_startup.html'
    model = FeedbackForStartup
    fields = '__all__'


@method_decorator(login_required, name='dispatch')
class MentoringListView(ListView):
    template_name = '../templates/mentoring/mentoring.html'
    model = Mentoring

    def get_context_data(self, **kwargs):
        context = super(MentoringListView, self).get_context_data(**kwargs)
        # context['mentoringData'] = Mentoring.objects.select_related('program').select_related('mentor').values('mentor__mentor__profile__picture','mentor__mentor__profile__nickname','mentor__mentor__profile__experiences__title','mentor__mentor__profile__experiences__company','program__description','datetime','title','id')
        # context['mentoringData'] = Mentoring.objects.select_related('program').select_related('mentor').values('mentor__mentor__profile__picture','mentor__mentor__profile__nickname','program__description','datetime','title','id')
        context['mentoringData'] = Mentoring.objects.select_related('program').select_related('mentor')
       
        return context

@method_decorator(login_required, name='dispatch')
class GenerateMentoringPdf(WeasyTemplateResponseMixin, ListView):
    template_name = '../templates/mentoring/generate-pdf.html'
    model = Mentoring

    def get_context_data(self, **kwargs):
        context = super(GenerateMentoringPdf, self).get_context_data(**kwargs)
       
        return context

@method_decorator(login_required, name='dispatch')
class MentoringDetailView(DetailView):
    template_name = '../templates/mentoring/detail.html'
    model = Mentoring

    def get_context_data(self, **kwargs):
        context = super(MentoringDetailView, self).get_context_data(**kwargs)
        context['dataMentoring'] = Mentoring.objects.get(id = self.kwargs['pk'])

        return context

class MentoringAttendance(ListView):
    template_name = '../templates/mentoring/attendance.html'
    model = Mentoring

    def get_context_data(self, **kwargs):
        context = super(MentoringAttendance, self).get_context_data(**kwargs)

        return context

@method_decorator(login_required, name='dispatch')
class MentoringFeedback(DetailView):
    template_name = '../templates/mentoring/mentoring_feedback.html'
    model = Mentoring

    def get_context_data(self, **kwargs):
        print(kwargs)
        context = super(MentoringFeedback, self).get_context_data(**kwargs)
        context['dataMentoring'] = Mentoring.objects.get(id = self.kwargs['pk'])
        user = self.request.user.id
        print(user)
        startup_member = self.request.user.startupmember_set.filter(member_id=user)
        print(startup_member)
        context['mentor']  = Mentor.objects.all()

        if startup_member:
            context['startupedit'] = startup_member
        # elif mentor:
            # context['mentoredit'] = mentor
        else:
            pass

        return context

@method_decorator(login_required, name='dispatch')
class MentoringCreateView(CreateView):
    template_name = '../templates/mentoring/mentoring_create.html'
    model = Mentoring
    fields = '__all__'

@method_decorator(login_required, name='dispatch')
class MentoringUpdateStartupView(UpdateView):
    template_name = '../templates/mentoring/mentoring_update.html'
    model = Mentoring
    form_class = StartupMentoringForm

    def get_context_data(self, **kwargs):
        context = super(MentoringUpdateStartupView, self).get_context_data(**kwargs)
        context['dataMentoring'] = Mentoring.objects.get(id = self.kwargs['pk'])
       
        return context

@method_decorator(login_required, name='dispatch')
class MentoringUpdateMentorView(UpdateView):
    template_name = '../templates/mentoring/mentoring_update.html'
    model = Mentoring
    # fields = ('mentor_notes',)
    form_class = MentorMentoringNoteForm

    def get_context_data(self, **kwargs):
        context = super(MentoringUpdateMentorView, self).get_context_data(**kwargs)
        context['dataMentoring'] = Mentoring.objects.get(id = self.kwargs['pk'])
       
        return context

@method_decorator(login_required, name='dispatch')
class MentoringDeleteView(DeleteView):
    model = Mentoring
    success_url = reverse_lazy('mentoring-list')

@method_decorator(login_required, name='dispatch')
class ScheduleListView(ListView):
    template_name = '../templates/schedule/schedule.html'
    model = Scheduling
    ordering = ['-created']

    def get_context_data(self, **kwargs):
        context = super(ScheduleListView, self).get_context_data(**kwargs)
        context['attend'] = Scheduling.objects.filter(status_startup=True).count()
        print(context['attend'])
        return context

@method_decorator(login_required, name='dispatch')
class ScheduleMentorAttendanceUpdate(View):
    def get(self, request, *args, **kwargs):
        schedule = Scheduling.objects.get(id=self.kwargs['pk'])
        schedule.status_mentor = True
        schedule.save()

        # return redirect('mentor-detail', slug=self.kwargs['slug'])
        return redirect('mentor-detail', pk=self.kwargs['pk'])


@method_decorator(login_required, name='dispatch')
class ScheduleStartupAttendanceUpdate(View):
    def get(self, request, *args, **kwargs):
        schedule = Scheduling.objects.get(id=self.kwargs['pk'])
        schedule.status_startup = True
        schedule.save()

        return redirect('startup-detail', slug=self.kwargs['slug']) 

class MentorTaskAdd(CreateView):
    template_name = '../templates/mentor/add_task.html'
    model = Task
    form_class = MentorTaskForm

    def get_success_url(self, *args, **kwargs):

        # mentoring = Mentoring.objects.get(mentor_id=self.request.user.mentor.id)
        # print(mentoring)
        # mentoringid = mentoring.id
        # print(mentoringid)
        return reverse_lazy('mentoring-list' )   


# @method_decorator(login_required, name='dispatch')
# class MentorProfileDetailView(TemplateView):
# 
    # template_name = '../templates/account/profile.html'
    # model = MentorProfile
# 
    # model = Mentor

    # def process_request(self, request):
    #     return User.objects.get(username=request.user.username)

        # def get_context_data(self, **kwargs):
        #     context = super(MentorProfileDetailView, self).get_context_data(**kwargs)
        #     if user.groups.get(name='mentor'):
        #         context['dataProfile'] = MentorProfile.objects.get(mentor__user__username=user)
        #     else:
        #         context['dataProfile'] = FounderProfile.objects.get(founder__user__username=user)
        #     return context
# 
    # def get_context_data(self, **kwargs):
    #     context = super(MentorProfileDetailView, self).get_context_data(**kwargs)
    #     user = User.objects.get(username=self.request.user.username)
    #     context['as'] = user

    #     if user.groups.filter(name='mentor'):
    #         # context['dataProfile'] = MentorProfile.objects.get(mentor__user__username=user)
    #         # context['dataProfile'] = Mentor.objects.get(mentor__user__username=user)
    #         context['dataProfile'] = Mentor.objects.get(mentor__user__username)
    #         a = Mentor.objects.all()
    #         context['a'] = a
    #         print(a)
    #         print(context['dataProfile'])
    #     elif user.groups.filter(name='startup'):
    #         context['dataProfile'] = FounderProfile.objects.get(founder__user__username=user)
    #     else:         
    #         pass
    #     return context 
# 

# class InviteMember(TemplateView):
#     invite = Invitation.create('email@example.com', inviter=request.user)
#     invite.send_invitation(request)   