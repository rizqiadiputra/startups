from .settings_base import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'dek(f=&1x$3rbq0tsyfl)ig^mu_&fdnr-$wo9o-ye!nr*=a!g7'

DEBUG= True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'startupsuccess_db',
        'USER': 'putra',
        'PASSWORD': 'root12345',
        'HOST': 'localhost',
        'PORT': '',
    }
}

STATIC_ROOT = '/var/www/startupsuccess/static'
MEDIA_ROOT = '/var/www/startupsuccess/media'

# # use console for dev
# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# use smtp
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'startupsuccessdev@gmail.com'
EMAIL_HOST_PASSWORD = 'Root_12345'
