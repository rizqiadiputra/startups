from .settings_base import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'dek(f=&1x$3rbq0tsyfl)ig^mu_&fdnr-$wo9o-ye!nr*=a!g7'

DEBUG=False

ALLOWED_HOSTS = ['*',]

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
#SECURE_SSL_REDIRECT = True
SESSION_COOKIE_SECURE = True
SESSION_COOKIE_HTTPONLY = True
CSRF_COOKIE_SECURE = True
USE_X_FORWARDED_HOST = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'startupsuccess-live',
        'USER': 'startupsuccess',
        'PASSWORD': 'D1G1T4R4Y4',
        'HOST': '35.198.225.24',
        'PORT': '5432',
    }
}

STATIC_ROOT = '/var/www/startupsuccess/static'
MEDIA_ROOT = '/var/www/startupsuccess/media'

# use smtp
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'startupsuccess@digitaraya.com'
EMAIL_HOST_PASSWORD = 'D1G1T4R4Y4'
