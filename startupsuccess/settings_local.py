from .settings_base import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'dek(f=&1x$3rbq0tsyfl)ig^mu_&fdnr-$wo9o-ye!nr*=a!g7'

DEBUG= True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'startupsuccess_db',
        'USER': 'root',
        'PASSWORD': 'root12345',
        'HOST': 'localhost',
        'PORT': '',
    }
}   

STATIC_ROOT =  os.path.join(BASE_DIR, "../startupsuccess-collectstatic/static")
MEDIA_ROOT = os.path.join(BASE_DIR, "../startupsuccess-collectstatic/media")

# use console for dev
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'