"""startupsuccess URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    # 1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# date = timezone.now()
#   print()
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from config.views import *
from ecosystem.views import *
from journal.views import *

# from material.admin import urls

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('admin/', admin.site.urls),
        path('accounts/', include('allauth.urls')),

        path('__debug__/', include(debug_toolbar.urls)),

        # path('profile/', MentorProfileDetailView.as_view(), name='myprofile'),
        # path('profile/<slug>/bio/add/', ProfileBio.as_view(), name='profile-bio'),
        
        #new user
        path('profile/<slug>/create', CreateProfile.as_view(), name='profile_create'),
        path('profile/<slug>/experience/', CreateProfileExperience.as_view(), name='profile_create_experience'),
        path('profile/<slug>/education/', CreateProfileEducation.as_view(), name='profile_create_education'),
        path('profile/<slug>/skill/', CreateProfileSkill.as_view(), name='profile_create_skill'),
        
        path('entities-role/', RoleEntitiesView.as_view(), name='role-entities'),
        path('mentor/add/', MentorAdd.as_view(), name='mentor-add'),
        path('mentor/qualification/', MentorQualification.as_view(), name='qualification_mentor'),
        path('mentor/qualification/add/', MentorQualificationAnswer.as_view(), name='qualification_add_mentor'),
        path('mentor/qualification/status/', MentorQualificationStatus.as_view(), name='qualification_status_mentor'),
        path('startup-role/', RoleStartupView.as_view(), name='role-startup'),
        path('startup/add/', StartupAdd.as_view(), name='startup-add'),


        #profile
        path('profile/<slug>/', ProfileView.as_view(), name='myprofile'),
        path('profile/<slug>/update/', ProfileUpdate.as_view(), name='profile-update-profile'),
        path('profile/<slug>/experience/add/', ProfileExperienceAdd.as_view(), name='profile_add_experience'),
        path('profile/<slug>/<pk>/experience/update/', ProfileExperienceUpdate.as_view(), name='profile_update_experience'),
        path('profile/<slug>/<pk>/experience/delete/', ProfileExperienceDelete.as_view(), name='profile_delete_experience'),
        path('profile/<slug>/education/add/', ProfileEducationAdd.as_view(), name='profile_add_education'),
        path('profile/<slug>/<pk>/education/update/', ProfileEducationUpdate.as_view(), name='profile_update_education'),
        path('profile/<slug>/<pk>/education/delete/', ProfileEducationDelete.as_view(), name='profile_delete_education'),
        path('profile/<slug>/skill/add', ProfileSkillAdd.as_view(), name='profile_add_skill'),
        path('profile/<slug>/<pk>/skill/update/', ProfileSkillUpdate.as_view(), name='profile_update_skill'),
        path('profile/<slug>/<pk>/skill/delete/', ProfileSkillDelete.as_view(), name='profile_delete_skill'),

        # path('profile/<slug>/update', ProfileUserUpdate.as_view(), name='profile-update-user'),


        #schedule
        path('schedule/', ScheduleListView.as_view(), name='schedule-list'),
        path('schedule/update/mentor/<slug>/<pk>/', ScheduleMentorAttendanceUpdate.as_view(), name='schedule_update_attendance_mentor'),
        path('schedule/update/startup/<slug>/<pk>/', ScheduleStartupAttendanceUpdate.as_view(), name='schedule_update_attendance_startup'),

        #mentoring
        path('mentor/<slug>/mentoring/add', MentorMentoringAdd.as_view(), name='mentor-add-mentoring'),
        path('mentor/<slug>/<pk>/mentoring/note', MentorMentoringNote.as_view(), name='mentoring_add_note'),
        path('startup/<slug>/<pk>/update/mentoring', StartupUpdateMentoring.as_view(), name='startup-update-mentoring'),

        #feedback
        path('mentoring/<pk>/feedback/add/startup/', StartupFeedbackForMentor.as_view(), name='startup-feedback'),
        path('mentoring/<pk>/feedback/add/mentor/', MentorFeedbackForStartup.as_view(), name='mentor-feedback'),

        #task
        path('mentor/<pk>/task/add', MentorTaskAdd.as_view(), name='mentor_add_task'),



        # path('startup/<slug>/mentoring/add', StartupMentoringAdd.as_view(), name='startup-add-mentoring'),



        # path('profile/<slug>/', ProfileExperienceAdd.as_view(), name='profile_experience_add'),

        # path('profile/<slug>/', ProfileView.as_view(), name='myprofile'),
        path('profile/<slug>/<pk>/', ApprovalMemberView.as_view(), name='approval-startup'),
        #2
        path('mentor/', MentorListView.as_view(), name='mentor-list'),
        path('mentor/<slug>/add/', MentorUpdateSkillsView.as_view(), name='mentor-update-skills'),
        #3
        # path('mentor/<slug>/', MentorDetailView.as_view(), name='mentor-detail'),
        path('mentor/<pk>/', MentorDetailView.as_view(), name='mentor-detail'),

        #4
        path('mentor/<slug>/generate-pdf', GenerateMentorPdf.as_view(), name='generate-mentor-pdf'),
        path('mentor/<slug>/update/', MentorUpdateView.as_view(), name='mentor-update'),
        path('mentor/<slug>/<pk>/update/mentoring', MentorUpdateMentoring.as_view(), name='mentor-update-mentoring'),
        path('mentor/<slug>/mentoring-detail', MentorMentoringDetail.as_view(), name='mentor-mentoring-detail'),

        path('member/invite/', MemberListView.as_view(), name='member-invite'),
        path('member/invite/<pk>', RequestInviteMemberView.as_view(), name='request-invite'),

        path('startup-role/requestjoin/<slug>', RequestJoinView.as_view(), name='join-startup'),

        path('startup/<slug>/<int:pk>/', ApprovalView.as_view(), name='approval-member' ),
        #0
        #6
        path('startup/', StartupListView.as_view(), name='startup-list'),
        path('startup/<slug>/', StartupDetailView.as_view(), name='startup-detail'),
        path('startup/<slug>/generate-pdf', GenerateStartupPdf.as_view(), name='generate-startup-pdf'),
        path('startup/<slug>/update/', StartupUpdate.as_view(), name='startup-update'),
        path('startup/<slug>/<pk>/mentoring-detail', StartupMentoringDetailView.as_view(), name='startup-mentoring-detail'),
        path('startup/<slug>/<pk>/add/answer-task', StartupMentoringAddTask.as_view(), name='startup-add-mentoring-task'),
        path('startup/<slug>/delete/', StartupDelete.as_view(), name='startup-delete'),
        path('startup/<slug>/add-progress', StartupAddProgress.as_view(), name='startup-add-progress'),
        path('startup/<slug>/<pk>/detail/progress', StartupDetailProgress.as_view(), name='startup-detail-progress'),
        path('startup/<slug>/<pk>/update/progress', StartupUpdateProgress.as_view(), name='startup-update-progress'),
        path('startup/<slug>/<pk>/delete/progress', StartupDeleteProgress.as_view(), name='startup-delete-progress'),
        path('startup/<slug>/<pk>/schedule-detail', StartupScheduleDetailView.as_view(), name='startup-schedule-detail'),

        path('mentoring/', MentoringListView.as_view(), name='mentoring-list'),
        path('mentoring/add/', MentoringCreateView.as_view(), name='mentoring-add'),
        path('mentoring/generate-pdf', GenerateMentoringPdf.as_view(), name='generate-mentoring-pdf'),
        #5
        path('mentoring/<pk>/', MentoringDetailView.as_view(), name='mentoring-detail'),
        path('mentoring/<pk>/attendance/', MentoringAttendance.as_view(), name='mentoring-attendance'),

        path('mentoring/<pk>/feedback', MentoringFeedback.as_view(), name='mentoring-feedback'),
        
        path('mentoring/<pk>/delete/', MentoringDeleteView.as_view(), name='mentoring-delete'),
        path('mentoring/<pk>/update/startup', MentoringUpdateStartupView.as_view(), name='mentoring-update-startup'),
        path('mentoring/<pk>/update/mentor', MentoringUpdateMentorView.as_view(), name='mentoring-update-mentor'),


        #1
        path('', HomeView.as_view(), name='home'),

        path(r'jet/', include('jet.urls', 'jet')),  # Django JET URLS
        # path('')
        path('invitations/', include('invitations.urls', namespace='invitations')),
        path('member/invite/email/', InviteEmailMemberView.as_view(), name='member-invite-email'),


    ]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
