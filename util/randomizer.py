import random
import string
from django.utils.text import slugify
from django.core.exceptions import FieldDoesNotExist

def random_string_generator(size=6, chars=string.ascii_lowercase + string.digits):
  return ''.join(random.choice(chars) for _ in range(size))

def unique_slug_generator_byfield(instance, slugable_field_name):
  slug = slugify(getattr(instance, slugable_field_name))
  return unique_slug_generator(instance, slug)

def unique_slug_generator(instance, customslug):
  """
  This is for a Django project and it assumes your instance 
  has a model with a slug field and a title character (char) field.
  """
  slug = slugify(customslug)
  unique_slug = "{slug}-{randstr}".format(randstr=random_string_generator(size=8, chars=string.hexdigits), slug=slug)
  Klass = instance.__class__

  while Klass.objects.filter(slug=unique_slug).exists():
    unique_slug = "{slug}-{randstr}".format(randstr=random_string_generator(size=8, chars=string.hexdigits), slug=slug)
  
  return unique_slug